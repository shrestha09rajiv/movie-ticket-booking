export enum Genre {
  COMEDY = 'comedy',
  THRILLER = 'thriller',
  ACTION = 'action',
  HORROR = 'horror',
}
